package com.example.bima.finalproject_KADE.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.bima.finalproject_KADE.R
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.Team
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.model.db.adapter.MainAdapter
import com.example.bima.finalproject_KADE.model.Player_Model
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import com.example.bima.finalproject_KADE.view.DetailSchedule
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class Previous_Match : Fragment(), MainInterface {


    private var event : MutableList<Event> = mutableListOf()
    private lateinit var swipe: SwipeRefreshLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var recycler: RecyclerView
    private lateinit var presenter: MainPresenter
    private lateinit var adater: MainAdapter
    private lateinit var spinner: Spinner

    var league: String = "4335"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view : View = UI {
           linearLayout {
               lparams(matchParent, wrapContent)
               orientation = LinearLayout.VERTICAL

               spinner = spinner {
                   id = R.id.spinners
               }

               swipe = swipeRefreshLayout {
                   relativeLayout {
                       lparams(matchParent, wrapContent)
                       recycler = recyclerView {
                           lparams(matchParent, wrapContent)
                           layoutManager =  LinearLayoutManager(ctx)
                       }
                       progressBar = progressBar{

                       }.lparams{
                           centerHorizontally()
                       }
                   }
               }
           }
       }.view

        adater = MainAdapter(event, 2) {
            ctx.startActivity<DetailSchedule>("nameHome" to "${it.strHomeTeam}","id" to "${it.idEvent}",  "nameAway" to "${it.strAwayTeam}",
                "schedule" to "previous_match","date" to "${it.strDate}", "time" to "${it.strTime}")
        }

        recycler.adapter = adater

        val request = ApiRespotory()
        val gson = Gson()
        presenter = MainPresenter(this,request, gson)


        var spinnerItems = resources.getStringArray(R.array.league)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item,spinnerItems)

        spinner.adapter = spinnerAdapter


        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                var spinerposition = spinner.selectedItemPosition
                league = when(spinerposition){
                    0 -> "4328"
                    1 -> "4331"
                    2 -> "4332"
                    3 -> "4334"
                    else -> "4335"
                }
                    presenter.getListLastEvent(league)
            }
        }

        swipe.onRefresh {
            presenter.getListLastEvent(league)
        }
        return view
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
       progressBar.visibility = View.INVISIBLE
     }

    override fun showAllListEvents(data: List<Event>) {
        swipe.isRefreshing = false
        event.clear()
        event.addAll(data)
        adater.notifyDataSetChanged()
    }

    override fun detailTeamHome(data: List<Team>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun detailTeamAway(data: List<Team>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showListPlayer(data: List<Player_Model>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}