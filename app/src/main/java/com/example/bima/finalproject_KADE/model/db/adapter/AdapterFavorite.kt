package com.example.bima.finalproject_KADE.model.db.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.model.Favorite
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class AdapterFavorite (private val favorite: List<Favorite>, private val listener: (Favorite) -> Unit)
    : RecyclerView.Adapter<ViewHolderr>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderr =
        ViewHolderr(Ui_Favorite().createView(AnkoContext.create(parent.context,parent)))

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: ViewHolderr, position: Int) {
      holder.bindItem(favorite[position], listener)
       if(favorite[position].skoreTeamHome.isNullOrEmpty()){
           holder.versus.text = "VS"
       }else {
           holder.versus.text = "${favorite[position].skoreTeamHome} - ${favorite[position].skoreTeamAway}"
       }
    }
}

  class ViewHolderr(view: View) : RecyclerView.ViewHolder(view){

        private val teamHome : TextView = view.find(R.id.teamHome)
        private val teamAway : TextView  = view.find(R.id.teamAway)
        val versus : TextView = view.find(R.id.versus)
        val tgl : TextView = view.find(R.id.tanggal)

      fun bindItem(favorite: Favorite, listener: (Favorite) -> Unit ){
          tgl.text = favorite.tanggal
          teamHome.text = favorite.nameTeamHome
          teamAway.text = favorite.nameTeamAway
          itemView.setOnClickListener {
              listener(favorite)
          }
      }

    }

  class Ui_Favorite : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui){
            cardView {
                lparams(matchParent, wrapContent)
                elevation = dip(4).toFloat()

                linearLayout {
                    orientation = LinearLayout.VERTICAL
                    padding = dip(8)

                    textView("Minggu 2018 23 04"){
                        id = R.id.tanggal
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                        textSize = 16F

                    }.lparams(matchParent, wrapContent){
                        bottomMargin = dip(8)
                    }
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL

                        textView("Manchester Utd"){
                            gravity = Gravity.RIGHT or Gravity.CENTER
                            id = R.id.teamHome
                            maxLines = 1
                            textSize = 19f
                        }.lparams(dip(0),dip(50)){
                            weight = 0.4f
                        }
                        textView("VS"){
                            backgroundResource = android.R.color.darker_gray
                            gravity = Gravity.CENTER
                            id = R.id.versus
                            maxLines = 1
                            textColor = R.color.colorAccent
                            textSize = 20f
                        }.lparams(dip(0), dip(50)){
                            weight = 0.2f
                            leftMargin =dip(8)
                            rightMargin=dip(8)
                        }
                        textView("Chelsea"){
                            gravity = Gravity.LEFT or Gravity.CENTER
                            id = R.id.teamAway
                            maxLines = 1
                            textSize =19f
                        }.lparams(dip(0),dip(50)){
                            weight = 0.4f
                        }
                    }.lparams(matchParent, wrapContent){
                        bottomMargin = dip(8)
                    }
                }.lparams(matchParent, wrapContent)

            }
        }
    }
}

