package com.example.bima.finalproject_KADE.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.bima.finalproject_KADE.model.db.adapter.AdapterTeamFavorite
import com.example.bima.finalproject_KADE.model.db.database
import com.example.bima.finalproject_KADE.model.FavoriteTeams
import com.example.bima.finalproject_KADE.view.DetailTeam
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavoriteTeamFragments : Fragment(), AnkoComponent<Context> {

    private var favorites : MutableList<FavoriteTeams> = mutableListOf()
    private lateinit var recycler : RecyclerView
    private lateinit var adapter : AdapterTeamFavorite
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = AdapterTeamFavorite(favorites){
            ctx.startActivity<DetailTeam>("teamId" to it.idTeam,"deskripsi" to it.stadium
                ,"logo" to it.imageTeam,"stadium" to it.description, "year" to it.formedYear, "team" to it.teamName,"league" to it.league)
        }

        recycler.adapter =  adapter

        showListFavoriteTeam()

        swipeRefreshLayout.onRefresh {
            showListFavoriteTeam()
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(requireContext()))
    }

    override fun createView(ui: AnkoContext<Context>): View {
        return with(ui){
            linearLayout {
                lparams(matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL

                swipeRefreshLayout = swipeRefreshLayout {
                    relativeLayout {
                        lparams(matchParent, wrapContent)
                        recycler = recyclerView {
                            lparams(matchParent, wrapContent)
                            layoutManager = LinearLayoutManager(ctx)
                        }
                    }
                }
            }
        }
    }

    private fun showListFavoriteTeam(){
        favorites.clear()
        context?.database?.use {
            swipeRefreshLayout.isRefreshing = false
            val result = select(FavoriteTeams.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<FavoriteTeams>())
            favorites .addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }
}