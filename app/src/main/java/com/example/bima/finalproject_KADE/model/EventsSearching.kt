package com.example.bima.finalproject_KADE.model

import com.example.bima.submission2_kotlinandroidkejar.Model.Event

data class EventsSearching (val event: List<Event>)