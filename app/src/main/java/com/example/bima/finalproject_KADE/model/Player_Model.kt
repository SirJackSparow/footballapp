package com.example.bima.finalproject_KADE.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player_Model(
        @SerializedName("idTeam")
        var idTeam: String?,
        @SerializedName("idPlayer")
        var idPlayer: String?,
        @SerializedName("strPlayer")
        var playerName: String?,
        @SerializedName("strPosition")
        var position: String?,
        @SerializedName("strCutout")
        var profil : String?,
        @SerializedName("strDescriptionEN")
        var description: String?,
        @SerializedName("strHeight")
        var height: String?,
        @SerializedName("strWeight")
        var weight: String?,
        @SerializedName("strFanart1")
        var poster:String?
):Parcelable