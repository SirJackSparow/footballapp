package com.example.bima.finalproject_KADE.model.db
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.bima.finalproject_KADE.model.Favorite
import com.example.bima.finalproject_KADE.model.FavoriteTeams
import org.jetbrains.anko.db.*

class DatabaseHelperSqlite(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "FavoriteSchedule.db",null,1){
    companion object {
        private var instance: DatabaseHelperSqlite? = null

        @Synchronized
        fun getInstance (ctx: Context): DatabaseHelperSqlite{
            if (instance == null ){
                instance = DatabaseHelperSqlite(ctx.applicationContext)
            }
            return instance as DatabaseHelperSqlite
        }
    }
    override fun onCreate(db: SQLiteDatabase) {
        //create database
        db.createTable(
            Favorite.TABLE_FAVORITE, true
         , Favorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Favorite.SKORETEAMHOME to TEXT,
            Favorite.SKORETEAMAWAY to  TEXT,
            Favorite.EVENTID to TEXT + UNIQUE,
            Favorite.NAMETEAMHOME to TEXT,
            Favorite.NAMETEAMAWAY to TEXT,
            Favorite.BADGETEAMHOME to TEXT,
            Favorite.BADGETEAMAWAY to TEXT,
            Favorite.TANGGAL to TEXT,
            Favorite.GOALTEAMHOME to TEXT,
            Favorite.GOALTEAMAWAY to TEXT,
            Favorite.GKTEAMHOME to TEXT,
            Favorite.GKTEAMAWAY to TEXT,
            Favorite.DEFTEAMHOME to TEXT,
            Favorite.DEFTEAMAWAY to TEXT,
            Favorite.MIDTEAMHOME to TEXT,
            Favorite.MIDTEAMAWAY to TEXT,
            Favorite.FWDTEAMHOME to TEXT,
            Favorite.FWDTEAMAWAY to TEXT,
            Favorite.SUBTEAMHOME to TEXT,
            Favorite.SUBTEAMAWAY to TEXT,
            Favorite.STRTIME to TEXT
        )
        //create database team favorite
        db.createTable(FavoriteTeams.TABLE_FAVORITE, true,
        FavoriteTeams.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            FavoriteTeams.TEAM_ID to TEXT + UNIQUE,
            FavoriteTeams.TEAM_NAME to TEXT,
            FavoriteTeams.IMAGE_TEAM to TEXT,
            FavoriteTeams.FORMED_YEAR to TEXT,
            FavoriteTeams.STADIUM to TEXT,
            FavoriteTeams.DESCRIPTION to TEXT,
            FavoriteTeams.LEAGUE to TEXT
            )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
     db.dropTable(Favorite.TABLE_FAVORITE, true)
     db.dropTable(FavoriteTeams.TABLE_FAVORITE, true)
    }
}

val Context.database : DatabaseHelperSqlite
      get() = DatabaseHelperSqlite.getInstance(applicationContext)