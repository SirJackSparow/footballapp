package com.example.bima.finalproject_KADE.model.db.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.R.id.team_badge
import com.example.bima.finalproject_KADE.R.id.team_name
import com.example.bima.submission2_kotlinandroidkejar.Model.Team
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*

class TeamsAdapter(private val teams: List<Team>, private val listener: (Team) -> Unit) : RecyclerView.Adapter<TeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        return TeamViewHolder(FootBallTeam_UI().createView(AnkoContext.create(parent.context,parent)))
      }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
      holder.bindItem(teams[position], listener)
    }

}

class FootBallTeam_UI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {
        linearLayout {
            lparams(matchParent, wrapContent)
            padding = dip(16)
            orientation = LinearLayout.HORIZONTAL

            imageView {
                id = R.id.team_badge
            }.lparams{
                width = dip(50)
                height = dip(50)
            }

            textView {
                textSize = 15f
                id = R.id.team_name
            }.lparams{
                margin = dip(15)
            }
        }
    }
}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val teamName: TextView = view.find(team_name)
    private val teamImage: ImageView = view.find(team_badge)

    fun bindItem(team: Team, listener: (Team)-> Unit){
        Picasso.get().load(team.strTeamBadge).into(teamImage)
        teamName.text = team.strTeam
        itemView.setOnClickListener { listener(team) }
    }

}