package com.example.bima.finalproject_KADE.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.bima.finalproject_KADE.model.db.adapter.AdapterFavorite
import com.example.bima.finalproject_KADE.model.Favorite
import com.example.bima.finalproject_KADE.model.db.database
import com.example.bima.finalproject_KADE.view.DetailSchedule
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavoriteFragment : Fragment(), AnkoComponent<Context>  {

    private var favorites : MutableList<Favorite> = mutableListOf()
    private lateinit var recycle : RecyclerView
    private lateinit var adapter : AdapterFavorite
    private lateinit var swipe : SwipeRefreshLayout


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = AdapterFavorite(favorites) {
            ctx.startActivity<DetailSchedule>("id" to "${it.eventID}", "nameHome" to "${it.nameTeamHome}", "nameAway" to "${it.nameTeamAway}",
                "schedule" to "next_match", "date" to "${it.tanggal}", "time" to "${it.strTime}")
        }

        recycle.adapter = adapter

        showListFavorite()

        swipe.onRefresh {
            showListFavorite()
        }
    }

    override fun onResume() {
        super.onResume()
        showListFavorite()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
     return createView(AnkoContext.create(requireContext()))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
      linearLayout {
          lparams(matchParent, wrapContent)
          orientation = LinearLayout.VERTICAL

          swipe = swipeRefreshLayout {
              relativeLayout {
                  lparams(matchParent, wrapContent)
                  recycle = recyclerView {
                      lparams(matchParent, wrapContent)
                      layoutManager = LinearLayoutManager(ctx)
                  }
              }
          }
       }
    }

    fun showListFavorite(){
        favorites.clear()
        context?.database?.use {
            swipe.isRefreshing = false
            val result = select(Favorite.TABLE_FAVORITE)
            val favorite = result.parseList(classParser<Favorite>())
            favorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }
}