package com.example.bima.finalproject_KADE.model.db.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.R.id.image_team
import com.example.bima.finalproject_KADE.R.id.name_team
import com.example.bima.finalproject_KADE.model.FavoriteTeams
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*

class AdapterTeamFavorite(private val favorite: List<FavoriteTeams>, private val listener : (FavoriteTeams) -> Unit)
    : RecyclerView.Adapter<ViewsHolderr> () {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewsHolderr {
        return ViewsHolderr(UI_TeamFavorite().createView(AnkoContext.create(parent.context,parent)))
    }

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: ViewsHolderr, position: Int) {
            holder.bindItem(favorite[position],listener)
    }
}

class UI_TeamFavorite : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(matchParent, wrapContent)
                padding = dip(16)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = R.id.image_team
                }.lparams{
                    width = dip(50)
                    height = dip(50)
                }

                textView {
                    textSize = 15f
                    id = R.id.name_team
                }.lparams{
                    margin = dip(15)
                }
            }
        }
    }
}


class ViewsHolderr(view: View) : RecyclerView.ViewHolder(view) {
    private  val teamName: TextView = view.find(name_team)
    private val teamImage: ImageView = view.find(image_team)

    fun bindItem(favorite: FavoriteTeams, listener: (FavoriteTeams) -> Unit){
        Picasso.get().load(favorite.imageTeam).into(teamImage)
        teamName.text = favorite.teamName
        itemView.setOnClickListener { listener(favorite) }
    }
}