package com.example.bima.finalproject_KADE.view

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.provider.CalendarContract
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.Team
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.R.id.add_to_scedulefavorite
import com.example.bima.finalproject_KADE.R.id.schedulers
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.model.Favorite
import com.example.bima.finalproject_KADE.model.db.database
import com.example.bima.finalproject_KADE.model.Player_Model
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_xml.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast
import java.text.SimpleDateFormat
import java.util.*

class DetailSchedule : AppCompatActivity(), MainInterface{

    private lateinit var req: ApiRespotory
    private lateinit var gson: Gson
    private lateinit var presenter: MainPresenter

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    private lateinit var events: Event
    private lateinit var id: String
    private lateinit var nameTeamHome: String
    private lateinit var nameTeamAway: String
    private lateinit var schedule: String
    private lateinit var date: String
    private lateinit var time: String

    private var badgeTeamHome:String? = null
    private var badgeTeamAway:String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_xml)

        val intent = intent
        id = intent.getStringExtra("id")
        nameTeamHome = intent.getStringExtra("nameHome")
        nameTeamAway = intent.getStringExtra("nameAway")
        schedule = intent.getStringExtra("schedule")
        date = intent.getStringExtra("date")
        time = intent.getStringExtra("time")


        req = ApiRespotory()
        gson = Gson()
        presenter = MainPresenter(this,req,gson)

        favoriteState()

        presenter.getDetailEventList(id)
        presenter.detailHomeTeam(nameTeamHome)
        presenter.detailAwayTeam(nameTeamAway)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_favorite,menu)
        menuItem = menu
        setFavorite()
        return true

    }

    fun setFavorite (){
      if(isFavorite){
          menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this,R.drawable.ic_added_to_favorites)
          toast( nameTeamHome + " - " + nameTeamAway )}
        else
          menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
            return when (item.itemId){
                android.R.id.home -> {
                    finish()
                    true
                }
                add_to_scedulefavorite -> {
                    //toast(item.itemId)
                    if (isFavorite) removeFromFavorite() else addToFAvorite(events)
                    isFavorite = !isFavorite
                    setFavorite()
                    true
                }
                schedulers -> {
                    if (schedule.contentEquals("next_match")){
                        val intent = Intent(Intent.ACTION_EDIT)
                            intent.type = "vnd.android.cursor.item/event"
                            intent.putExtra(CalendarContract.Events.TITLE, "$nameTeamHome  VS  $nameTeamAway")
                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,formatTimeDate(date,time))
                            .putExtra(CalendarContract.Events.ALL_DAY,false)
                        toast(events.strDate!!)
                        startActivity(intent)

                    }else{
                        toast("Pertandingan ini sudah selesai")
                    }
                    true
                }
                else -> super.onOptionsItemSelected(item)
            }
    }

    private fun addToFAvorite(event: Event) {
        try {
            database.use {
                insert(
                    Favorite.TABLE_FAVORITE,
                    Favorite.EVENTID to event.idEvent,
                    Favorite.NAMETEAMHOME to event.strHomeTeam,
                    Favorite.NAMETEAMAWAY to event.strAwayTeam,
                    Favorite.BADGETEAMHOME to badgeTeamHome,
                    Favorite.BADGETEAMAWAY to badgeTeamAway,
                    Favorite.TANGGAL to event.strDate,
                    Favorite.SKORETEAMHOME to event.skorHome,
                    Favorite.SKORETEAMAWAY to event.skoreAway,
                    Favorite.GOALTEAMHOME to event.strHomeGoalDetail,
                    Favorite.GOALTEAMAWAY to event.strAwayGoalDetail,
                    Favorite.GKTEAMHOME to event.lineHomeupGoalKeper,
                    Favorite.GKTEAMAWAY to event.lineupAwayGoalKeeper,
                    Favorite.DEFTEAMHOME to event.lineupHomeDefense,
                    Favorite.DEFTEAMAWAY to event.lineupAwayDefense,
                    Favorite.MIDTEAMHOME to event.lineupHomeMidField,
                    Favorite.MIDTEAMAWAY to event.lineupAwayMidField,
                    Favorite.FWDTEAMHOME to event.lineupHomeForward,
                    Favorite.FWDTEAMAWAY to event.lineupAwayForward,
                    Favorite.SUBTEAMHOME to event.lineupHomeSub,
                    Favorite.SUBTEAMAWAY to event.lineupAwaySub,
                    Favorite.STRTIME to time
                )
            }
        }catch (e: SQLiteConstraintException){
        }
    }

    private fun removeFromFavorite() {
      try {
          database.use {
              delete(Favorite.TABLE_FAVORITE, "(EVENT_ID) = {id}","id" to id)
          }
      }catch (e: SQLiteConstraintException){

      }
    }

    private fun favoriteState():Boolean{
        database.use {
            val result = select(Favorite.TABLE_FAVORITE)
                .whereArgs("(EVENT_ID = {id})",
                    "id" to id)
            val favorite = result.parseList(classParser<Favorite>())
            if (!favorite.isEmpty())
                isFavorite = true
        }
        return isFavorite
    }

    override fun showLoading() {
       progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress.visibility = View.INVISIBLE
    }

    override fun showAllListEvents(data: List<Event>) {
       events =  Event(data[0].idEvent, data[0].strHomeTeam, data[0].strAwayTeam, data[0].strDate,
           data[0].skorHome, data[0].skoreAway, data[0].lineHomeupGoalKeper, data[0].lineupAwayGoalKeeper,
           data[0].lineupHomeDefense, data[0].lineupAwayDefense, data[0].lineupHomeMidField, data[0].lineupAwayMidField,
           data[0].lineupHomeForward,data[0].lineupAwayForward, data[0].lineupHomeSub, data[0].lineupAwaySub, data[0].strHomeGoalDetail
           ,data[0].strAwayGoalDetail )

        if (data[0].skorHome != null){
            LastGame.visibility = View.VISIBLE
            txtvNextGame.visibility = View.GONE

            txtvSkor.text = "${data[0].skorHome + " - " + data[0].skoreAway}"
            //goal
            txtvGoalHome.text = data[0].strHomeGoalDetail
            txtvGoalAway.text = data[0].strAwayGoalDetail
            //GKLineup
            txtvHomeGKLineup.text = data[0].lineHomeupGoalKeper
            txtvAwayGKLineup.text = data[0].lineupAwayGoalKeeper
            //deflineup
            txtvHomeDefender.text = data[0].lineupHomeDefense
            txtvAwayDefender.text = data[0].lineupAwayDefense
            //midlineup
            txtvHomeMid.text = data[0].lineupHomeMidField
            txtvAwayMid.text = data[0].lineupAwayMidField
            //forwardlineup
            txtvHomeForward.text = data[0].lineupHomeForward
            txtvAwayForward.text = data[0].lineupHomeForward
            //subline
            txtvHomeSub.text = data[0].lineupHomeSub
            txtvAwaySub.text = data[0].lineupAwaySub

        }
        else {
            txtvNextGame.text = "Pertandingan dimulai tanggal   ${data[0].strDate}"
            LastGame.visibility = View.GONE
            txtvNextGame.visibility = View.VISIBLE
        }

    }

    private  fun formatTimeDate(date: String, time: String) : Long{
        val formatted = SimpleDateFormat("yyyy-MM-dd HH:mm:ssXXX", Locale.US)
        val dates = formatted.parse("$date $time")
        val calendar = Calendar.getInstance()
        calendar.time = dates
        return calendar.timeInMillis
    }
    override fun detailTeamHome(data: List<Team>) {
        txtvHome.text = data[0].strTeam
        Picasso.get().load(data[0].strTeamBadge).into(imVHome)
        badgeTeamHome = data[0].strTeamBadge
    }

    override fun detailTeamAway(data: List<Team>) {
     txtvAway.text = data[0].strTeam
        Picasso.get().load(data[0].strTeamBadge).into(imVAway)
        badgeTeamAway = data[0].strTeamBadge
    }

    override fun showListPlayer(data: List<Player_Model>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}