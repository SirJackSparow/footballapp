package com.example.bima.finalproject_KADE.view

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.view.fragment.Next_Match
import com.example.bima.finalproject_KADE.view.fragment.Previous_Match
import org.jetbrains.anko.*
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.themedTabLayout
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.UI
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.viewPager

class FragmentSchedule :  Fragment() {

    private lateinit var tab : TabLayout
    private lateinit var pager: ViewPager
    private lateinit var searcing: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view : View = UI {
            linearLayout {
                lparams(matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL

                appBarLayout {
                    lparams(matchParent, wrapContent)

                    tab = themedTabLayout(R.style.ThemeOverlay_AppCompat_Dark){
                        lparams(matchParent, wrapContent) {
                            tabGravity = Gravity.NO_GRAVITY
                            tabMode = TabLayout.MODE_FIXED
                        }
                    }
                }
                searcing = imageView {
                    setImageResource(R.drawable.ic_search_black_24dp)
                    onClick {
                        startActivity<Searching>("status" to "events")
                    }
                }.lparams(matchParent, dip(34)){
                    gravity = Gravity.LEFT
                }

                pager = viewPager{
                    id = R.id.pagerst
                }.lparams(matchParent, wrapContent)
            }
            setViewPager(pager)
            tab.setupWithViewPager(pager)
        }.view

        return view
    }

    //nested fragment "childfragmentmanager"
    private fun setViewPager(viewPager: ViewPager){
        val pager = AdapterViewPager(childFragmentManager)
        viewPager.adapter =  pager
    }

    class AdapterViewPager internal constructor(fm: FragmentManager) : FragmentPagerAdapter(fm){

        override fun getItem(position: Int): Fragment? {
            var fragment: Fragment? = null
            if (position == 0){
              fragment = Next_Match()
            }else if (position == 1){
                fragment = Previous_Match()
            }
           return fragment
        }

        override fun getCount(): Int = 2

        override fun getPageTitle(position: Int): CharSequence? {
            var title : String? = null
            if (position == 0){
                title = "Next Match"
            }else if (position == 1){
                title = "Last Match"
            }
            return title
        }

    }

}