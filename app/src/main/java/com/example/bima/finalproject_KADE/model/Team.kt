package com.example.bima.submission2_kotlinandroidkejar.Model

import com.google.gson.annotations.SerializedName

data class Team (
    @SerializedName("strTeam")
    var strTeam: String ,
    @SerializedName("strTeamBadge")
    var strTeamBadge: String,
    @SerializedName("idTeam")
    var teamId: String ,
    @SerializedName("intFormedYear")
    var teamFormedYear: String,

    @SerializedName("strStadium")
    var teamStadium: String,

    @SerializedName("strDescriptionEN")
    var teamDescription: String,

    @SerializedName("strManager")
    var coach:String,

    @SerializedName("strLeague")
    var strLeague:String

)