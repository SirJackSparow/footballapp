package com.example.bima.submission2_kotlinandroidkejar.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Event (
    @SerializedName("idEvent")
    var idEvent: String? = null,
    @SerializedName("strHomeTeam")
    var strHomeTeam: String,
    @SerializedName("strAwayTeam")
    var strAwayTeam: String,
    @SerializedName("dateEvent")
    var strDate: String? = null,
    @SerializedName("intHomeScore")
    var skorHome: String? = null,
    @SerializedName("intAwayScore")
    var skoreAway: String? = null,
    @SerializedName("strHomeLineupGoalkeeper")
    var lineHomeupGoalKeper: String? = null,
    @SerializedName("strHomeLineupDefense")
    var lineupHomeDefense: String? = null,
    @SerializedName("strHomeLineupMidfield")
    var lineupHomeMidField: String? = null,
    @SerializedName("strHomeLineupForward")
    var lineupHomeForward: String? = null,
    @SerializedName("strHomeLineupSubstitutes")
    var lineupHomeSub: String? = null,
    @SerializedName("strHomeGoalDetails")
    var strHomeGoalDetail: String? = null,
    @SerializedName("strAwayLineupGoalkeeper")
    var lineupAwayGoalKeeper: String? = null,
    @SerializedName("strAwayLineupDefense")
    var lineupAwayDefense: String? = null,
    @SerializedName("strAwayLineupMidfield")
    var lineupAwayMidField: String? = null,
    @SerializedName("strAwayLineupForward")
    var lineupAwayForward: String? = null,
    @SerializedName("strAwayLineupSubstitutes")
    var lineupAwaySub: String? = null,
    @SerializedName("strAwayGoalDetails")
    var strAwayGoalDetail: String? = null,
    @SerializedName("strTime")
    var strTime: String? = null
):Parcelable