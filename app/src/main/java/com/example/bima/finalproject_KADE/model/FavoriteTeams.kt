package com.example.bima.finalproject_KADE.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FavoriteTeams( val id: Long?, val idTeam: String?, val teamName: String?, val imageTeam: String?,
                      val formedYear: String?, val description: String?, val stadium: String?, val league: String?):Parcelable {
    companion object {
        const val TABLE_FAVORITE: String = "FAVORITE_TEAM"
        const val ID: String = "ID_"
        const val TEAM_ID = "TEAM_ID"
        const val TEAM_NAME: String = "TEAM_NAME"
        const val IMAGE_TEAM: String = "IMAGE_TEAM"
        const val FORMED_YEAR:String = "FORMED_YEAR"
        const val DESCRIPTION: String = "DESCRIPTION"
        const val STADIUM : String = "STADIUM"
        const val LEAGUE : String = "LEAGUE"
    }
}