package com.example.bima.finalproject_KADE.model.db.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bima.finalproject_KADE.R
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import java.text.SimpleDateFormat
import java.util.*

class MainAdapter(private val events: List<Event>, private val no: Int, private val listener: (Event)-> Unit)
    : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(UiEvent().createView(AnkoContext.create(parent.context,parent)))

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // if akan dieksekusi keduannya
        holder.bindItem(events[position], listener)
        if (no != 1) {
          holder.versuss.text ="${events[position].skorHome} - ${events[position].skoreAway}"
        }
        if (events[position].skorHome.isNullOrEmpty() || events[position].skoreAway.isNullOrEmpty()){
            holder.versuss.text = "VS"
        }
    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
    private val teamHome: TextView = view.find(R.id.teamHome)
    private val teamAway: TextView = view.find(R.id.teamAway)
    val versuss : TextView = view.find(R.id.versus)
     val tgl: TextView = view.find(R.id.tanggal)

    fun bindItem(event: Event, listener: (Event) -> Unit){
     tgl.text = event.strDate
     teamHome.text = event.strHomeTeam
     teamAway.text = event.strAwayTeam
     itemView.setOnClickListener {
         listener(event)
     }
    }

}

class UiEvent : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui){
            cardView {
                lparams(matchParent, wrapContent)
                elevation = dip(4).toFloat()

                linearLayout {
                    orientation = LinearLayout.VERTICAL
                    padding = dip(8)

                    textView("Minggu 2018 23 04"){
                        id = R.id.tanggal
                        textAlignment = View.TEXT_ALIGNMENT_CENTER
                        textSize = 16F

                    }.lparams(matchParent, wrapContent){
                       bottomMargin = dip(8)
                    }

                  linearLayout {
                      orientation = LinearLayout.HORIZONTAL

                      textView("Manchester Utd"){
                          gravity = Gravity.RIGHT or Gravity.CENTER
                          id = R.id.teamHome
                          maxLines = 1
                          textSize = 19f
                      }.lparams(dip(0),dip(50)){
                          weight = 0.4f
                      }
                      textView("VS"){
                          backgroundResource = android.R.color.darker_gray
                          gravity = Gravity.CENTER
                          id = R.id.versus
                          maxLines = 1
                          textColor = R.color.colorAccent
                          textSize = 20f
                      }.lparams(dip(0), dip(50)){
                          weight = 0.2f
                          leftMargin =dip(8)
                          rightMargin=dip(8)
                      }
                      textView("Chelsea"){
                         gravity = Gravity.LEFT or Gravity.CENTER
                         id = R.id.teamAway
                          maxLines = 1
                          textSize =19f
                      }.lparams(dip(0),dip(50)){
                          weight = 0.4f
                      }
                  }.lparams(matchParent, wrapContent){
                      bottomMargin = dip(8)
                  }
                }.lparams(matchParent, wrapContent)

            }
        }
    }
}