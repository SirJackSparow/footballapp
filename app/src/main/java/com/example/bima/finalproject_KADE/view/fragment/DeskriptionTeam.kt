package com.example.bima.finalproject_KADE.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import org.jetbrains.anko.*

class DeskriptionTeam : Fragment(), AnkoComponent<Context> {


    private  var deskription: String? = null

    companion object {
        fun newInstance(deskription: String) : DeskriptionTeam{
            val argum = Bundle()
            argum.putString("deskription", deskription)
            val deskription = DeskriptionTeam()
            deskription.arguments = argum
            return deskription
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        deskription = arguments?.getString("deskription")
        return createView(AnkoContext.create(requireContext()))
    }



    override fun createView(ui: AnkoContext<Context>): View {
        return with(ui){
            scrollView {
                lparams(matchParent, wrapContent)


                    textView {
                        R.id.deskriptionss
                        textSize = 17f
                        text = deskription
                    }.lparams(matchParent, wrapContent){
                    }

            }
        }
    }

}