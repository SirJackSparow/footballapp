package com.example.bima.finalproject_KADE.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.model.db.adapter.TeamsAdapter
import com.example.bima.finalproject_KADE.model.Player_Model
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import com.example.bima.finalproject_KADE.view.DetailTeam
import com.example.bima.finalproject_KADE.view.Searching
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.Team
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class TeamFragment : Fragment(), AnkoComponent<Context>, MainInterface {

    private  var teams: MutableList<Team> = mutableListOf()
    private lateinit var presenter: MainPresenter
    private lateinit var adapter : TeamsAdapter
    private lateinit var spinner: Spinner
    private lateinit var recycler: RecyclerView
    private lateinit var progress: ProgressBar
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var league: String

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val itemSpinners = resources.getStringArray(R.array.league)
        val adapterSpiner = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_dropdown_item,itemSpinners)

        spinner.adapter = adapterSpiner

        adapter = TeamsAdapter(teams){
            //toast(it.strTeam)
            ctx.startActivity<DetailTeam>("teamId" to it.teamId,"deskripsi" to it.teamDescription
                ,"logo" to it.strTeamBadge,"stadium" to it.teamStadium, "year" to it.teamFormedYear, "team" to it.strTeam,"league" to league)

        }
        recycler.adapter = adapter

        val request = ApiRespotory()
        val gson = Gson()
        presenter = MainPresenter(this, request,gson)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                league = spinner.selectedItem.toString()
                presenter.getTeams(league)
            }

            override fun onNothingSelected(parent: AdapterView<*>?): Unit =
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }
        swipeRefreshLayout.onRefresh {
            presenter.getTeams(league)
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(requireContext()))
    }

    override fun createView(ui: AnkoContext<Context>): View {
      return with(ui){
          linearLayout {
              lparams(matchParent, wrapContent)
              orientation = LinearLayout.VERTICAL

              imageView {
                  setImageResource(R.drawable.ic_search_black_24dp)
                  onClick {
                      startActivity<Searching>("status" to "teams")
                  }
              }.lparams(matchParent, dip(34)){
                  gravity = Gravity.LEFT
              }
              spinner = spinner {
                  id = R.id.spinner
              }
            swipeRefreshLayout = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light)

                relativeLayout {
                    lparams(matchParent, wrapContent)

                    recycler = recyclerView {
                        lparams(matchParent, wrapContent)
                        id = R.id.list_team
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progress = progressBar {
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }

          }
      }
    }

    override fun showLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
      progress.visibility = View.INVISIBLE
    }

    override fun showAllListEvents(data: List<Event>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun detailTeamHome(data: List<Team>) {
        swipeRefreshLayout.isRefreshing = false
        teams.clear()
        teams.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun detailTeamAway(data: List<Team>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun showListPlayer(data: List<Player_Model>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}