package com.example.bima.finalproject_KADE.view

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.R.id.add_to_scedulefavorites
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.model.db.database
import com.example.bima.finalproject_KADE.model.FavoriteTeams
import com.example.bima.finalproject_KADE.model.Player_Model
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import com.example.bima.finalproject_KADE.view.fragment.DeskriptionTeam
import com.example.bima.finalproject_KADE.view.fragment.Player_Fragment
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.Team
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_team.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class DetailTeam : AppCompatActivity(), MainInterface {

    private lateinit var req: ApiRespotory
    private lateinit var gson: Gson
    private lateinit var presenter: MainPresenter

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    private lateinit var team: Team
    lateinit var teamId: String
    private lateinit var league : String
    private lateinit var description: String
    private lateinit var logo: String
    private lateinit var stadium: String
    private lateinit var year:String
    private lateinit var teamName:String

    private lateinit var  ImgV: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.detail_team)
        setSupportActionBar(toolbar)
        super.onCreate(savedInstanceState)
        val intent = intent
        teamId = intent.getStringExtra("teamId")
        description = intent.getStringExtra("deskripsi")
        logo = intent.getStringExtra("logo")
        year = intent.getStringExtra("year")
        stadium = intent.getStringExtra("stadium")
        teamName = intent.getStringExtra("team")
        league = intent.getStringExtra("league")



        ImgV = findViewById(R.id.img_logo)
        Picasso.get().load(logo).into(ImgV)
        team_names.text = teamName
        yearformed.text = year
        stadiums.text = stadium

        req =  ApiRespotory()
        gson = Gson()
        presenter = MainPresenter(this,req,gson)

        presenter.getDetailsTeams(teamId)
        favoriteState()

        setViewPager(view_Pager)
        tablayout.setupWithViewPager(view_Pager)

    }



   private fun setViewPager(viewPager: ViewPager){
        val pager = AdapterViewPagers(supportFragmentManager, teamId, description)
        viewPager.adapter = pager
    }


    class AdapterViewPagers internal constructor(fm: FragmentManager, teamId : String, deskription: String) : FragmentPagerAdapter(fm){
        private val id = teamId
        private val deskriptions = deskription

        override fun getItem(position: Int): Fragment? {
          var fragment: Fragment? = null
            if (position == 0){
                fragment = DeskriptionTeam.newInstance(deskriptions)
            }else if (position == 1){
                fragment = Player_Fragment.newInstance(id)
            }
            return fragment
        }

        override fun getCount(): Int = 2

        override fun getPageTitle(position: Int): CharSequence? {
            var title : String? =null
            if (position == 0){
                title = "Overview"
            }else if (position == 1){
                title = "Player"
            }
            return title
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_favorite_team, menu)
        menuItem = menu
        setFavorites()
        return true
    }

   private fun setFavorites(){
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        }else{
            menuItem?.getItem(0)?.icon =ContextCompat.getDrawable(this,R.drawable.ic_add_to_favorites)
        }
    }

   private fun removeFavorite(){
        try {
            database.use {
                delete(FavoriteTeams.TABLE_FAVORITE, "(TEAM_ID = {id} ) ", "id" to teamId)
            }
        }catch (e : SQLiteConstraintException){
            toast(e.localizedMessage)
        }

    }

   private fun addFavorite(){
        try {
            database.use {
                insert(FavoriteTeams.TABLE_FAVORITE,
                    FavoriteTeams.TEAM_ID to teamId,
                            FavoriteTeams.TEAM_NAME to teamName,
                    FavoriteTeams.IMAGE_TEAM to logo,
                    FavoriteTeams.FORMED_YEAR to year,
                    FavoriteTeams.STADIUM to stadium,
                    FavoriteTeams.LEAGUE to league,
                    FavoriteTeams.DESCRIPTION to description)
            }
        }catch (e: SQLiteConstraintException){}

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
             android.R.id.home -> {
                 finish()
                 true
             }
            add_to_scedulefavorites -> {
                if (isFavorite) removeFavorite() else addFavorite()
                isFavorite = !isFavorite
                setFavorites()
                true
            }else -> super.onOptionsItemSelected(item)
        }
    }

    // cek apakah data sdah ada di database atau belum
     private fun favoriteState ()  {
        database.use {
            val result = select(FavoriteTeams.TABLE_FAVORITE)
                .whereArgs("TEAM_ID = {id}",
                    "id" to teamId)
            val favorite = result.parseList(classParser<FavoriteTeams>())
            if (!favorite.isEmpty())
                isFavorite = true
        }
    }

    override fun showLoading() {
        progresst.visibility = View.VISIBLE
    }

    override fun hideLoading() {
       progresst.visibility = View.INVISIBLE
    }

    override fun showAllListEvents(data: List<Event>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showListPlayer(data: List<Player_Model>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun detailTeamHome(data: List<Team>) {
       team = Team(data[0].strTeam,data[0].strTeamBadge,data[0].teamId,data[0].teamFormedYear,data[0].teamStadium,data[0].teamDescription
           ,data[0].coach,data[0].strLeague)
       /* Picasso.get().load(data[0].strTeamBadge).into(img_logo)
       team_names.text = data[0].strTeam
       yearformed.text = data[0].teamFormedYear
       stadiums.text = data[0].teamStadium */
    }

    override fun detailTeamAway(data: List<Team>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}