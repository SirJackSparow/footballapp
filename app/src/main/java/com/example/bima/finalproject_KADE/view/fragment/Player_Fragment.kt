package com.example.bima.finalproject_KADE.view.fragment
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.model.db.adapter.PlayerAdapter
import com.example.bima.finalproject_KADE.model.Player_Model
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import com.example.bima.finalproject_KADE.view.DetailPlayers
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.Team
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class Player_Fragment : Fragment(), AnkoComponent<Context>, MainInterface {

    companion object {
        fun newInstance(teamId: String) : Player_Fragment{
            val argum = Bundle()
            argum.putString("idTeam", teamId)
            val player_frag = Player_Fragment()
            player_frag.arguments = argum
            return player_frag
        }
    }

    private var player: MutableList<Player_Model> = mutableListOf()
    private lateinit var presenter: MainPresenter
    private lateinit var adapter : PlayerAdapter
    private lateinit var recycler: RecyclerView
    private lateinit var progres: ProgressBar
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        adapter = PlayerAdapter(player){
          ctx.startActivity<DetailPlayers>("player" to it)
        }

        recycler.adapter = adapter

        val team = arguments?.getString("idTeam")

        val request = ApiRespotory()
        val gson = Gson()
        presenter = MainPresenter(this,request, gson)

        if (team != null) {
            presenter.getPlayer(team)
        }


        swipeRefreshLayout.onRefresh {
            if (team != null) {
                presenter.getPlayer(team)
            }
        }
    }


    override fun createView(ui: AnkoContext<Context>): View  {
        return with(ui){
            linearLayout {
                lparams(matchParent, wrapContent)
                orientation = LinearLayout.VERTICAL

                swipeRefreshLayout = swipeRefreshLayout{
                    setColorSchemeResources(
                        R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                    relativeLayout {
                        lparams(matchParent, wrapContent)

                        recycler = recyclerView {
                            lparams(matchParent, wrapContent)
                            id = R.id.players
                            layoutManager = LinearLayoutManager(ctx)
                        }

                        progres = progressBar {
                        }.lparams{
                            centerHorizontally()
                        }
                    }
                }

            }
        }
    }

    override fun showLoading() {
      progres.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progres.visibility = View.INVISIBLE
    }

    override fun showAllListEvents(data: List<Event>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showListPlayer(data: List<Player_Model>) {
        swipeRefreshLayout.isRefreshing = false
        player.clear()
        player.addAll(data)
        adapter.notifyDataSetChanged()
    }

    override fun detailTeamHome(data: List<Team>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun detailTeamAway(data: List<Team>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(requireContext()))
    }
}