package com.example.bima.finalproject_KADE.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Favorite(val id : Long?,val skoreTeamHome : String?, val skoreTeamAway : String?,val eventID : String?, val nameTeamHome : String?,
                    val nameTeamAway : String?, val badgeTeamHome : String?,  val badgeTeamAway : String?, val tanggal : String?,
                    val goalTeamHome : String?, val goalTeamAway : String?, val gkTeamHome : String?, val gkTeamAway: String?,
                    val defTeamHome:String?, val defTeamAway: String?, val midTeamHome: String?, val midTeamAway: String?,
                    val fwdTeamHome: String?, val fwdTeamAway:String?, val subTeamHome:String?, val subTeamAway:String?, val strTime:String?
                    ):Parcelable {
    companion object {
        const val TABLE_FAVORITE : String = "TABLE_FAVORITE"
        const val ID : String = "ID_"
        const val SKORETEAMHOME : String = "SKOR_HOME"
        const val SKORETEAMAWAY : String = "SKORE_AWAY"
        const val EVENTID : String = "EVENT_ID"
        const val NAMETEAMHOME : String = "NAME_TEAM_HOME"
        const val NAMETEAMAWAY : String = "NAME_TEAM_AWAY"
        const val BADGETEAMHOME : String = "BADGE_TEAM_HOME"
        const val BADGETEAMAWAY : String = "BADGE_TEAM_AWAY"
        const val TANGGAL : String = "TANGGAL"
        const val GOALTEAMHOME : String = "GOALTEAMHOME"
        const val GOALTEAMAWAY : String = "GOALTEAMAWAY"
        const val GKTEAMHOME : String = "GKTEAMHOME"
        const val GKTEAMAWAY : String = "GKTEAMAWAY"
        const val DEFTEAMHOME : String = "DEFTEAMHOME"
        const val DEFTEAMAWAY : String = "DEFTEAMAWAY"
        const val MIDTEAMHOME : String = "MIDTEAMHOME"
        const val MIDTEAMAWAY : String = "MIDTEAMAWAY"
        const val FWDTEAMHOME : String = "FWDTEAMHOME"
        const val FWDTEAMAWAY : String = "FWDTEAMAWAY"
        const val SUBTEAMHOME : String = "SUBTEAMHOME"
        const val SUBTEAMAWAY : String = "SUBTEAMAWAY"
        const val STRTIME: String = "STRTIME"
    }
}