package com.example.bima.submission2_kotlinandroidkejar.Model

data class TeamResponse (val teams: List<Team>)