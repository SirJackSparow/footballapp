package com.example.bima.finalproject_KADE.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.model.db.adapter.MainAdapter
import com.example.bima.finalproject_KADE.model.db.adapter.TeamsAdapter
import com.example.bima.finalproject_KADE.model.Player_Model
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.Team
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_searching.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class Searching : AppCompatActivity(), MainInterface {

    private var events: MutableList<Event> = mutableListOf()
    private var team : MutableList<Team> = mutableListOf()

    private lateinit var presenter: MainPresenter
    private lateinit var teamAdapter: TeamsAdapter
    private lateinit var eventsAdapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searching)

        val request = ApiRespotory()
        val gson = Gson()
        presenter = MainPresenter(this, request, gson)

        val intent = intent
        val strStatus = intent.getStringExtra("status")
        toast(strStatus)
        statusChecked(strStatus)
    }

    private fun statusChecked (status: String){
        if (status.contentEquals("events")){

            eventsAdapter = MainAdapter(events, 0 ){
                startActivity<DetailSchedule>("nameHome" to "${it.strHomeTeam}","id" to "${it.idEvent}",  "nameAway" to "${it.strAwayTeam}",
                    "schedule" to "next_match","date" to "${it.strDate}", "time" to "${it.strTime}")
                toast("${it.skorHome} - ${it.skoreAway}").show()
            }

            recyclerSearch.layoutManager = LinearLayoutManager(this)
            recyclerSearch.adapter = eventsAdapter

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    presenter.getSearchingEventList(query)
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    presenter.getSearchingEventList(newText)
                    return true
                }
            })
        }else if(status.contentEquals("teams")){
           teamAdapter = TeamsAdapter(team){
               startActivity<DetailTeam>("teamId" to it.teamId,"deskripsi" to it.teamDescription
               ,"logo" to it.strTeamBadge,"stadium" to it.teamStadium, "year" to it.teamFormedYear, "team" to it.strTeam,"league" to it.strLeague)
           }

            recyclerSearch.layoutManager = LinearLayoutManager(this)
            recyclerSearch.adapter = teamAdapter

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String): Boolean {
                   presenter.getSearchingTeam(query)
                    return  true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                   presenter.getSearchingTeam(newText)
                    return false
                }
            })


        }
    }

    override fun showLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideLoading() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showAllListEvents(data: List<Event>) {
     events.clear()
     events.addAll(data)
     eventsAdapter.notifyDataSetChanged()
    }

    override fun showListPlayer(data: List<Player_Model>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun detailTeamHome(data: List<Team>) {
      team.clear()
        team.addAll(data)
        teamAdapter.notifyDataSetChanged()
    }

    override fun detailTeamAway(data: List<Team>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}