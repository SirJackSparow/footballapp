package com.example.bima.finalproject_KADE.presenter

import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.DBApi
import com.example.bima.submission2_kotlinandroidkejar.Model.EventResponse
import com.example.bima.submission2_kotlinandroidkejar.Model.TeamResponse
import com.example.bima.finalproject_KADE.CoroutineContextProvider
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.model.EventsSearching
import com.example.bima.finalproject_KADE.model.Player_Response
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainPresenter( private val view:  MainInterface,private val APIrespository: ApiRespotory, private val gson: Gson ,
                     private val context: CoroutineContextProvider = CoroutineContextProvider()
) {

    //searching presenter
    fun getSearchingEventList(searcEvent: String){
        GlobalScope.launch(context.main) {
            val data  = gson.fromJson(APIrespository
                .doRequest(DBApi.getSearchEvent(searcEvent)).await(),
                EventsSearching::class.java)

            view.showAllListEvents(data.event)
        }
    }

    fun getSearchingTeam(team: String){
        GlobalScope.launch(context.main) {
            val data = gson.fromJson(APIrespository
                .doRequest(DBApi.getSearchTeam(team)).await(),
                TeamResponse::class.java)

            view.detailTeamHome(data.teams)
        }
    }

    fun getPlayer (team: String){
        view.showLoading()

        GlobalScope.launch(context.main) {
            val data = gson.fromJson(APIrespository
                .doRequest(DBApi.getListPlayer(team)).await(),
                Player_Response::class.java)

            view.hideLoading()
            view.showListPlayer(data.player)
        }
    }

    fun getTeams (league: String){
        view.showLoading()

        GlobalScope.launch(context.main) {
            val data = gson.fromJson(APIrespository
                    .doRequest(DBApi.getListTeams(league)).await(),
                    TeamResponse::class.java)

            view.hideLoading()
            view.detailTeamHome(data.teams)
        }

    }


    fun getListNextEvent (league: String){
        view.showLoading()

        GlobalScope.launch(context.main) {
            val data = gson.fromJson(APIrespository
                    .doRequest(DBApi.getNextMatch(league)).await(),
                    EventResponse::class.java )

            view.hideLoading()
            view.showAllListEvents(data.events)
        }
    }

    fun getListLastEvent (league: String){
        view.showLoading()

        GlobalScope.launch(context.main){
            val data = gson.fromJson(APIrespository
                    .doRequest(DBApi.getPastMatch(league)).await(),
                    EventResponse::class.java)

            view.hideLoading()
            view.showAllListEvents(data.events)
        }
    }

    fun getDetailEventList(event: String){
        view.showLoading()

        GlobalScope.launch(context.main){
            val data = gson.fromJson(APIrespository
                    .doRequest(DBApi.detailEventss(event)).await(),
                    EventResponse::class.java)

            view.hideLoading()
            view.showAllListEvents(data.events)
        }
    }

    fun detailHomeTeam (name: String ){
        view.showLoading()

        GlobalScope.launch(context.main){
            val data = gson.fromJson(APIrespository
                    .doRequest(DBApi.detailTeam(name)).await(),
                    TeamResponse::class.java)

            view.hideLoading()
            view.detailTeamHome(data.teams)
        }

    }

    fun getDetailsTeams (teamID: String){
        view.showLoading()

        GlobalScope.launch(context.main) {
            val data = gson.fromJson(APIrespository
                .doRequest(DBApi.getDetailsTeams(teamID)).await(),
                TeamResponse::class.java)

            view.hideLoading()
            view.detailTeamHome(data.teams)
        }
    }

    fun detailAwayTeam (name: String){
        view.showLoading()

        GlobalScope.launch(context.main) {
            val data = gson.fromJson(APIrespository
                    .doRequest(DBApi.detailTeam(name)).await(),
                    TeamResponse::class.java)

            view.hideLoading()
            view.detailTeamAway(data.teams)
        }
      }
}