package com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.net.URL

// untuk medapatkan response dari api
class ApiRespotory {
    fun doRequest(url: String) : Deferred<String> = GlobalScope.async {
        URL(url).readText()
    }
}