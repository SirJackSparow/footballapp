package com.example.bima.submission2_kotlinandroidkejar.Model

data class EventResponse (val events: List<Event>)