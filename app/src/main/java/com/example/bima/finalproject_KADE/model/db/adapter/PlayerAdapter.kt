package com.example.bima.finalproject_KADE.model.db.adapter

import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.R.id.*
import com.example.bima.finalproject_KADE.model.Player_Model
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*

class PlayerAdapter (private val player: List<Player_Model>, private val listener: (Player_Model) -> Unit) : RecyclerView.Adapter<PlayerViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder {
       return PlayerViewHolder(Player_UI().createView(AnkoContext.create(parent.context,parent)))
    }

    override fun getItemCount(): Int = player.size

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
       holder.bindItem(player[position],listener)
    }

}

class Player_UI : Fragment(), AnkoComponent<ViewGroup>{

    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui){
            linearLayout {
                lparams(matchParent, wrapContent)
                padding =  dip(16)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = R.id.images_player
                }.lparams{
                    width = dip(50)
                    height = dip(50)
                }

                textView {
                  id = R.id.name_players
                  textSize = 15f
                }.lparams{
                    margin = dip(15)
                }

                textView {
                    id = R.id.position
                    textSize = 15f
                }.lparams{
                    margin = dip(15)
                }
            }
        }
    }
}

class PlayerViewHolder(view : View) : RecyclerView.ViewHolder(view){

    val namePlayer: TextView = view.find(name_players)
    val imagePlayer : ImageView = view.find(images_player)
    val position : TextView = view.find(R.id.position)

    fun bindItem(player: Player_Model,listener: (Player_Model) -> Unit){
        Picasso.get().load(player.profil).into(imagePlayer)
        namePlayer.text = player.playerName
        position.text = player.position
        itemView.setOnClickListener { listener(player) }

    }

}