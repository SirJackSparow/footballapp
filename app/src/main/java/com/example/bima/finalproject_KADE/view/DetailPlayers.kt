package com.example.bima.finalproject_KADE.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.model.Player_Model
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_details_player.*

class DetailPlayers : AppCompatActivity()  {

    private lateinit var player : Player_Model

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details_player)

        val intent = intent
        player = intent.getParcelableExtra("player")

        Picasso.get().load(player.poster).into(ImagesBadge)
        txtposition.text = player.position
        txtWeight.text = player.weight
        txtheight.text = player.height
        txtDescription.text = player.description



    }
}