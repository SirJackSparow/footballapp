package com.example.bima.finalproject_KADE.view.`interface`

import com.example.bima.finalproject_KADE.model.Player_Model
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.Team

interface MainInterface {

    fun showLoading()
    fun hideLoading()
    fun showAllListEvents(data : List<Event>)
    fun showListPlayer(data: List<Player_Model>)
    fun detailTeamHome(data : List<Team>)
    fun detailTeamAway(data : List<Team>)

}