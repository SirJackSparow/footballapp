package com.example.bima.finalproject_KADE.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import com.example.bima.finalproject_KADE.R
import com.example.bima.finalproject_KADE.R.id.*
import com.example.bima.finalproject_KADE.view.fragment.*
import kotlinx.android.synthetic.main.activity_home.*
class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        supportActionBar?.title = "Football Schedule"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        bottom_navigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
            next_match -> {
            loadNextMatch(savedInstanceState)
            }
            prev_match -> {
            teamList(savedInstanceState)
            }
            favorite -> {
                loadFavoriteSchedule(savedInstanceState)
            }
            teamfavorite -> {
                loadFavoriteTeam(savedInstanceState)
            }
          }
            true
        }
        bottom_navigation.selectedItemId = next_match
    }

    private fun loadFavoriteTeam(savedInstanceState: Bundle?){
        if (savedInstanceState == null){
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_container, FavoriteTeamFragments(), FavoriteTeamFragments::class.simpleName)
                .commit()
        }
    }

    private fun loadNextMatch(savedInstanceState: Bundle?){
        if (savedInstanceState == null){
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_container, FragmentSchedule(), FragmentSchedule::class.simpleName)
                .commit()
        }
    }

    private fun loadFavoriteSchedule(savedInstanceState: Bundle?){
        if (savedInstanceState == null){
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_container, FavoriteFragment(), FavoriteFragment::class.simpleName)
                .commit()
        }
    }
    private fun teamList(savedInstanceState: Bundle?){
        if(savedInstanceState == null){
         supportFragmentManager
             .beginTransaction()
             .replace(R.id.main_container, TeamFragment(),TeamFragment::class.simpleName)
             .commit()
        }
    }
}
