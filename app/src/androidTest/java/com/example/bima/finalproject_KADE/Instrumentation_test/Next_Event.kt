package com.example.bima.finalproject_KADE.Instrumentation_test

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.example.bima.finalproject_KADE.R.id.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.v7.widget.RecyclerView
import com.example.bima.finalproject_KADE.view.HomeActivity
import android.support.test.espresso.matcher.ViewMatchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class Next_Event {
    @Rule
    @JvmField var activityRule = ActivityTestRule(HomeActivity::class.java)

    @Test
    fun testRecyclerView(){
        onView(withId(list_item)).check(matches(isDisplayed()))
        onView(withId(list_item)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(list_item)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(10, click()))
    }

    /*
    * 1. menampilkan bottom navigation
    * 2. click bottom navigation dengan id prev_match
    * 3. menampilkan jadwal dengan text arsenal
    * 4. clik jadwal dengan text arsenal
    * 5. menampilkan tombol favorite dengan id add_to_schedulefavorite
    * 6. click tombol favorite
    * 7. back
    * 8. menampilkan bottom navigation
    * 9. click bottom navigation dengan id next_match
    * 10.scroll sampai posisi 10
    * 11. menampilkan jadwal dengan text cardiff
    * 12. clik jadwal dengan text cardiff
    * 13. clik tombol favorite dengan id add_favorite_schedulefavorite
    * 14. back
    * 15. menampilkan bottom navigation
    * 16. click bottom navigation dengan id favorite
    * */

    @Test
    fun testingBehavior(){


        onView(withId(bottom_navigation)).check(matches(isDisplayed()))
        onView(withId(prev_match)).perform(click())
        onView(withText("Arsenal")).check(matches(isDisplayed()))
        onView(withText("Arsenal")).perform(click())

        onView(withId(add_to_scedulefavorites)).check(matches(isDisplayed()))
        onView(withId(add_to_scedulefavorites)).perform(click())

        pressBack()

        onView(withId(bottom_navigation)).check(matches(isDisplayed()))
        onView(withId(next_match)).perform(click())

        onView(withId(list_item)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withText("Everton")).check(matches(isDisplayed()))
        onView(withText("Everton")).perform(click())

        onView(withId(add_to_scedulefavorite)).perform(click())

        pressBack()

        onView(withId(bottom_navigation)).check(matches(isDisplayed()))

        onView(withId(favorite)).perform(click())
        Thread.sleep(3000)
    }
}