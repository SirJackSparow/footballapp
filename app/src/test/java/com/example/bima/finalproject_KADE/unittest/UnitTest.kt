package com.example.bima.finalproject_KADE.unittest

import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.ApiRespotory
import com.example.bima.submission2_kotlinandroidkejar.Model.DataAPI.DBApi
import com.example.bima.submission2_kotlinandroidkejar.Model.Event
import com.example.bima.submission2_kotlinandroidkejar.Model.EventResponse
import com.example.bima.finalproject_KADE.view.`interface`.MainInterface
import com.example.bima.finalproject_KADE.presenter.MainPresenter
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class UnitTest {


    @Mock
    private lateinit var view : MainInterface

    @Mock
    private lateinit var gson: Gson

    @Mock
    private lateinit var apiRespotory: ApiRespotory

    private lateinit var presenter: MainPresenter

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenter =  MainPresenter(view,apiRespotory, gson)
    }

    @Test
    fun testMainList() {
     val data : MutableList<Event> = mutableListOf()
     val response = EventResponse(data)

     GlobalScope.launch {
         `when`(gson.fromJson(apiRespotory
             .doRequest(DBApi.getNextMatch("4328")).await(),
             EventResponse::class.java
         )).thenReturn(response)

         presenter.getListNextEvent("4328")

         Mockito.verify(view).showLoading()
         Mockito.verify(view).showAllListEvents(data)
         Mockito.verify(view).hideLoading()
      }
    }
}