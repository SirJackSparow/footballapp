package com.example.bima.finalproject_KADE.utils

import android.annotation.SuppressLint
import org.junit.Test

import org.junit.Assert.*
import java.text.SimpleDateFormat
import java.util.*

class UtilsTest {

    @SuppressLint("SimpleDateFormat")
    fun toSinpleString(date: Date?) : String? = with(date ?: Date()){
        SimpleDateFormat("EEE, dd/MM/yyy").format(this)
    }
    @Test
    fun testToSinpleString() {
        val dateFormat  = SimpleDateFormat("MM/dd/yyyy")
        val date =  dateFormat.parse("02/28/2018")
        assertEquals("Wed, 28/02/2018",toSinpleString(date))
    }

}